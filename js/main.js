var totalResults = 0;
var media  = [];
var videoExtension = ["mp4"];
var audioExtension = ["mp3"];

/**
 * [mediaContent Crea y carga los elementos de audio y Video en el DOM
 *               uno por uno ]
 * @param  {[Array]} media [Con el los audios]
 * @return {[type]}       [description]
 */
function mediaContent(media){

  var thisFn = arguments.callee;
  
  if( media && media.length > 0){
    
    var file    = media[0].split("/");
    var fileName = file[1];
  
  	var fileExtension = fileName.split(".");
  		fileExtension = fileExtension[1];
  	
    var typeElement = "audio";
    var className   = "audio-item";
    var contentType   = "audio/mpeg";
    if(videoExtension.indexOf(fileExtension) != -1){
        typeElement = "video";
        className   = "video-item";
        contentType = "video/mpeg";
    }
    
    var element = document.createElement(typeElement);
        if( typeElement == 'video'){
            element.width  = "0";
            element.height = "0";
        }
        element.src = media[0];
        element.id  = fileName;
        element.className = className;
        element.type = contentType;
        
        media.shift();

        /*loadeddata*/
        element.addEventListener("loadeddata",function(){

          if(element.readyState) 
           var rest  = (totalResults - media.length);
           var width = Math.round((rest * 100)/totalResults);
           
           updateAvance(width+"%")
           //call recursive
           thisFn(media);

        },false);
       
        jQuery("#reproductor").append(element);

  }else{
      //completed all media files
      jQuery("#modalLoading").modal("hide");
      var firstAudio  = document.getElementById("d02-01.mp3");
      var secondAudio = document.getElementById("d02-02.mp3");
      if(firstAudio && secondAudio){
	   
	      firstAudio.play();
	      firstAudio.onended = function(){
			     secondAudio.play();      	
	      }
      }
  }
}

function updateAvance(width){

	jQuery("#progress-bar","#modalLoading").css({width:width});
	jQuery("#avance","#modalLoading").html(width);          

    return true;
}

function loadResources(){
  
  jQuery("#modalLoading").modal("show");

  var data = ["audios/d02-01.mp3","audios/d02-02.mp3","audios/d03.mp3","audios/d04-01.mp3","audios/d04-02.mp3","audios/d04-03.mp3","audios/d04-04.mp3",
              "audios/d06.mp3","audios/d07-01.mp3","audios/d07-02.mp3","audios/d07-03.mp3","audios/d07-04.mp3","audios/d07-05.mp3","audios/d07-06.mp3",
              "audios/d08-01.mp3","audios/d08-02.mp3","audios/d08-03.mp3","audios/d08-04.mp3","audios/d08-05.mp3","audios/d09-01.mp3","audios/d09-02.mp3",
              "audios/d09-03.mp3","audios/d09-04.mp3","audios/d09-05.mp3","audios/d09-06.mp3","audios/d10-01.mp3","audios/d10-02.mp3","audios/d11-01.mp3",
              "audios/d11-02.mp3","audios/d12-01.mp3","audios/d12-02.mp3","audios/d13-01.mp3","audios/d13.mp3","audios/d14-01.mp3","audios/d14-02.mp3",
              "audios/d14-03.mp3","audios/d14-04.mp3","audios/d14-05.mp3","audios/d16-01.mp3","audios/d16-02.mp3","audios/d16-03.mp3","audios/d16-04.mp3",
              "audios/d17-01.mp3","audios/d17.mp3","audios/d20-01.mp3","audios/d20-02.mp3","audios/d28-01.mp3","audios/d28.mp3","audios/d28-01.mp3","audios/d29.mp3",
              "audios/d30.mp3","audios/d31.mp3","audios/d32.mp3","audios/d33.mp3","audios/d34.mp3","audios/d35.mp3","audios/d36.mp3","audios/d38.mp3",
              "audios/d40.mp3","audios/d41-correcto.mp3","audios/d41-incorrecto.mp3","audios/d41.mp3","audios/d42-correcto.mp3",
              "audios/d42-incorrecto.mp3","audios/d42.mp3","audios/d44-correcto.mp3","audios/d44-incorrecto.mp3","audios/d44.mp3","audios/d45.mp3","audios/d46.mp3",
              "audios/d47.mp3","audios/d48.mp3","audios/d49.mp3"];



  totalResults =  data.length;
  mediaContent(data);
	
}

function loadPage(url){     
    stopAll(); 
    jQuery("#container-curso").load(url,function(){
    jQuery("#header-content").show();
  });
}

function stopAll(){

  var elmentsAudio = document.getElementsByClassName("audio-item");
  var elmentsVideo = document.getElementsByClassName("video-item");    
   
  for (var i = 0; i < elmentsAudio.length; i++) {
       //if(elmentsAudio[i].currentTime > 0){
           elmentsAudio[i].pause();
           elmentsAudio[i].currentTime = 0;
       //}
  }
  
  for (var y = 0; y < elmentsVideo.length; y++) {
      //if(elmentsVideo[y].currentTime >0){
          elmentsVideo[y].pause();
          elmentsVideo[y].currentTime = 0;
      //}
   
  }


  return true;

}

jQuery(document).ready(function(){
 	  
    jQuery("#modalLoading").modal({
        keyboard:false,
        backdrop:'static'
    });

    jQuery("#container-curso").delegate("a:not(.notClicked)","click",function(event){
       
        event.preventDefault();
        event.stopPropagation();

        if(!jQuery(this).hasClass('notClicked')){          
          var href = jQuery(this).attr("href");
          loadPage(href);
        }

    });

    jQuery("#container-curso").delegate("a.notClicked","click",function(event){
        event.preventDefault();
        return;
    });


});