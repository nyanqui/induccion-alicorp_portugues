const gulp = require('gulp');
const gulpLoadPlugins = require('gulp-load-plugins');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');

const filenamesToJson = require('gulp-filenames-to-json');
const browserSync = require('browser-sync').create();
const $ = gulpLoadPlugins();
const reload = browserSync.reload;

gulp.task('json', function () {
    console.log("here");
    return gulp.src('./audios/*')
        .pipe(filenamesToJson({fileName:"media.json"}))
        .pipe(gulp.dest('./'));
    

});


gulp.task('default', ['serve']);
